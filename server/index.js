require('dotenv').config();
const express = require("express");
const cors = require('cors');


const PORT = process.env.PORT || 3001;

const indexRouter = require('./routes/index.routes');
const aboutMeRouter = require('./routes/aboutMe.routes');
const photographyRouter = require('./routes/photography.routes');
const socialInfoRouter = require('./routes/socialInfo.routes');

const app = express();

app.use(cors());
app.use(express.json());

app.use('/', indexRouter);
app.use('/data/aboutMe', aboutMeRouter);
app.use('/data/photography', photographyRouter);
app.use('/data/socialInfo', socialInfoRouter);

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`)});