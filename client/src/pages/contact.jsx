import ContactForm from "../components/ContactForm";
import "../styles/contact.scss";

function Contact({theme}) {
  return (
    <main className={`Contact__${theme}`}>
      <section className="Contact__section">
        <h3>Contact</h3>
        <ContactForm />
      </section>
    </main>
  );
}
export default Contact;
